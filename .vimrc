" vimplug install plugins
call plug#begin('~/.vim/plugged')
Plug 'arcticicestudio/nord-vim'
Plug 'itchyny/lightline.vim'
Plug 'jiangmiao/auto-pairs'
Plug 'eiginn/netrw'
Plug 'joshdick/onedark.vim'
Plug 'mountain-theme/Mountain'

call plug#end()

" syntax highlighting
syntax on
colorscheme nord
" colorscheme onedark
"colorscheme mountain
set termguicolors

" set text width (80 char on a line before \n)
" set tab length
" tabs will not behave like spaces
" shift width when blocks are moved for indentation
set tw=80 ts=4 sts=0 sw=4
" filetype settings
au BufRead,BufNewFile *.c setlocal textwidth=80
autocmd Filetype html setlocal ts=2 sts=2 sw=2
autocmd Filetype css setlocal ts=2 sts=2 sw=2
autocmd Filetype javascript setlocal ts=2 sts=2 sw=2
autocmd Filetype python setlocal expandtab tw=80 sts=4 sw=4

" relative line numbers
set relativenumber

" highlighting with cursor doesn't get line numbers
set mouse+=a

" highlights current line
set cursorline

" hit enter to enter command mode (:)
nnoremap <CR> :

" Map space and \ as leader keys
let mapleader="\<Space>"
let maplocalleader="\\"

" Use <space><space> to toggle to the last buffer 
" open multiple files and switch between them
" also use :n
nnoremap <leader><leader> <c-^>

" lightline config
" lightline and uses vim-fugitive for git status
set laststatus=2
let g:lightline = {
      \ 'colorscheme': 'nord',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'fugitive#head',
      \   'filename': 'LightLineFilename'
      \ },
      \ }
function! LightLineFilename()
  return expand('%:p')
endfunction

" turn off vim bell
set noerrorbells visualbell t_vb=
if has('autocmd')
  autocmd GUIEnter * set visualbell t_vb=
endif

if &term == "alacritty"        
  let &term = "xterm-256color"
endif

