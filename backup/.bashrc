#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='[\u@\h \W]\$ '
eval "$(starship init bash)"

source "$HOME/.config/shell/env"
source "$HOME/.config/shell/alias"
