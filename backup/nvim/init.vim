" vimplug
call plug#begin()
" nord theme
Plug 'arcticicestudio/nord-vim',
" html expander
Plug 'mattn/emmet-vim',
" vim status bar
Plug 'itchyny/lightline.vim',
" git wrapper
Plug 'tpope/vim-fugitive', 
" create bracket pairs
Plug 'jiangmiao/auto-pairs'
call plug#end()

" set colorscheme
syntax on
colorscheme nord

" 24 bit colors
set termguicolors

" use "+y in visual mode to copy to system clipboard
" use "+p to paste from system clipboard
set clipboard=unnamedplus

" stop auto commenting
"autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" custom indentation settings
autocmd FileType html setlocal ts=2 sts=2 sw=2
autocmd FileType css setlocal ts=2 sts=2 sw=2
autocmd FileType javascript setlocal ts=2 sts=2 sw=2

" set text width and tab spaces
set tw=79 ts=4 sts=0 sw=4 "expandtab add this to python in the future

" enable line numbers
set relativenumber

" highlighting with cursor doesn't get line numbers
set mouse+=a

" hit enter to enter command mode (:)
nnoremap <CR> :

" highlight current line
set cursorline

" highlight 80 char ruler
set colorcolumn=80

" turn off vim bell
set noerrorbells visualbell t_vb=
if has('autocmd')
  autocmd GUIEnter * set visualbell t_vb=
endif



" lightline
set laststatus=2
let g:lightline = {
      \ 'colorscheme': 'nord',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'fugitive#head',
      \   'filename': 'LightLineFilename'
      \ },
      \ }
function! LightLineFilename()
  return expand('%:p')
endfunction

" set activator key for html plugin
let g:user_emmet_leader_key=','
